//
//  ManacalaLobbyMessages.swift
//
//  Created by Michael Sanford on 12/22/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore
import GamingCloudCore
import MancalaCore
import SwiftOnSockets

public struct MancalaServerPlayerInfo: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public let type: PlayerType
    public let info: MancalaPlayerInfo
    
    public init(type: PlayerType, info: MancalaPlayerInfo) {
        self.type = type
        self.info = info
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let type = PlayerType.init(unarchiver: unarchiver),
            let info = MancalaPlayerInfo.init(unarchiver: unarchiver) else { return nil }
        self.type = type
        self.info = info
    }
    
    public func encode(with archiver: ByteArchiver) {
        type.encode(with: archiver)
        info.encode(with: archiver)
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [MancalaServerPlayerInfo] type=\(type) | info=\(info) }"
    }
}

/*------------------------------------------------------------------------*/

public struct GameStartupInfo: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public let player1Info: MancalaServerPlayerInfo
    public let player2Info: MancalaServerPlayerInfo
    public let startingLogic: StartingTurnLogicType
    
    public init(player1Info: MancalaServerPlayerInfo, player2Info: MancalaServerPlayerInfo, startingLogic: StartingTurnLogicType) {
        self.player1Info = player1Info
        self.player2Info = player2Info
        self.startingLogic = startingLogic
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let player1Info = MancalaServerPlayerInfo(unarchiver: unarchiver),
            let player2Info = MancalaServerPlayerInfo(unarchiver: unarchiver),
            let startingLogic = StartingTurnLogicType(unarchiver: unarchiver) else { return nil }
        
        self.player1Info = player1Info
        self.player2Info = player2Info
        self.startingLogic = startingLogic
    }
    
    public func encode(with archiver: ByteArchiver) {
        player1Info.encode(with: archiver)
        player2Info.encode(with: archiver)
        startingLogic.encode(with: archiver)
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [GameStartupInfo] player1Info=\(player1Info) | player2Info=\(player2Info) | startingLogic=\(startingLogic) }"
    }
    
    public func playerInfo(forPlayerID playerID: PlayerID) -> MancalaPlayerInfo {
        return playerID == 0 ? player1Info.info : player2Info.info
    }
}

/*------------------------------------------------------------------------*/

public struct MancalaGameClientInfo: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public let startingPlayer: PlayerIdentity
    
    public init(startingPlayer: PlayerIdentity) {
        self.startingPlayer = startingPlayer
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let startingPlayer = PlayerIdentity(unarchiver: unarchiver) else { return nil }
        self.startingPlayer = startingPlayer
    }
    
    public func encode(with archiver: ByteArchiver) {
        startingPlayer.encode(with: archiver)
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [MancalaGameClientInfo] startingPlayer=\(startingPlayer) }"
    }
}
