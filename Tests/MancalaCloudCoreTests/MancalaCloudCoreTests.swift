import XCTest
import MancalaCore
import GamingCore
import NetworkingCore
@testable import MancalaCloudCore

class MancalaCloudCoreTests: XCTestCase {
    
    private var playerInfo1: MancalaServerPlayerInfo {
        return MancalaServerPlayerInfo(type: .human, info: MancalaPlayerInfo(playerName: "mike😂", gameboardID: "thing.board", score: 2000))
    }
    
    private var playerInfo2: MancalaServerPlayerInfo {
        return MancalaServerPlayerInfo(type: .bot(.intermediate), info: MancalaPlayerInfo(playerName: "john", gameboardID: "thing.board2", score: 3000))
    }
    
    private var gameToken1: GameToken {
        return GameToken(gameID: 123, watermark: 45678, playerID: 0)
    }
    
    private var gameToken2: GameToken {
        return GameToken(gameID: 123, watermark: 45678, playerID: 1)
    }
    
    func testCreateGameRequestCoding() {
        let preRequest = CreateGameRequest(player1Info: playerInfo1, player2Info: playerInfo2, startingLogic: .toggle)
        let archiver = ByteArchiver()
        preRequest.encode(with: archiver)
        let unarchiver = ByteUnarchiver(archive: archiver.archive)
        guard let postRequest = CreateGameRequest.init(unarchiver: unarchiver) else {
            XCTAssert(false, "Failed to unarchive CreateGameRequest")
            return
        }
        XCTAssert(preRequest == postRequest, "CreateGameRequest are not equal")
    }
    
    func testCreateGameResponseCoding() {
        let preResponse1 = CreateGameResponse(player1GameToken: gameToken1)
        let preResponse2 = CreateGameResponse(player1GameToken: gameToken1, player2GameToken: gameToken2)
        let archiver = ByteArchiver()
        preResponse1.encode(with: archiver)
        preResponse2.encode(with: archiver)
        let unarchiver = ByteUnarchiver(archive: archiver.archive)
        guard let postResponse1 = CreateGameResponse.init(unarchiver: unarchiver),
            let postResponse2 = CreateGameResponse.init(unarchiver: unarchiver) else {
            XCTAssert(false, "Failed to unarchive CreateGameResponse")
            return
        }
        XCTAssert(postResponse1 == postResponse1, "CreateGameResponse's are not equal")
        XCTAssert(postResponse2 == postResponse2, "CreateGameResponse's are not equal")
    }
    
    func testBatchCreateGameRequestCoding() {
        let preRequest0 = CreateGameRequest(player1Info: playerInfo1, player2Info: playerInfo2, startingLogic: .toggle)
        let preRequest1 = CreateGameRequest(player1Info: playerInfo2, player2Info: playerInfo1, startingLogic: .random)
        let preBatch = BatchCreateGameRequest(requestID: UUID(), createGameInfos: [0: preRequest0, 1: preRequest1])
        let archiver = ByteArchiver()
        preBatch.encode(with: archiver)
        let unarchiver = ByteUnarchiver(archive: archiver.archive)
        guard let postBatch = BatchCreateGameRequest.init(unarchiver: unarchiver) else {
            XCTAssert(false, "Failed to unarchive BatchCreateGameRequest")
            return
        }
        XCTAssert(preBatch == postBatch, "BatchCreateGameRequest's are not equal")
    }
    
    func testBatchCreateGameResponseCoding() {
        let response0 = CreateGameResponse(player1GameToken: gameToken1)
        let response1 = CreateGameResponse(player1GameToken: gameToken1, player2GameToken: gameToken2)
        let preBatch = BatchCreateGameResponse(requestID: UUID(), createGameResponses: [0: response0, 1: response1])
        let archiver = ByteArchiver()
        preBatch.encode(with: archiver)
        let unarchiver = ByteUnarchiver(archive: archiver.archive)
        guard let postBatch = BatchCreateGameResponse.init(unarchiver: unarchiver) else {
            XCTAssert(false, "Failed to unarchive BatchCreateGameRequest")
            return
        }
        XCTAssert(preBatch == postBatch, "BatchCreateGameRequest's are not equal")
    }
    
    static var allTests = [
        ("testCreateGameRequestCoding", testCreateGameRequestCoding),
        ("testCreateGameResponseCoding", testCreateGameResponseCoding),
        ("testBatchCreateGameRequestCoding", testBatchCreateGameRequestCoding),
        ("testBatchCreateGameResponseCoding", testBatchCreateGameResponseCoding),
    ]
}

extension MancalaServerPlayerInfo: Equatable {}
public func ==(lhs: MancalaServerPlayerInfo, rhs: MancalaServerPlayerInfo) -> Bool {
    switch (lhs.type, rhs.type) {
    case (.human, .human): ()
    case (.bot(let lhsLevel), .bot(let rhsLevel)):
        guard lhsLevel == rhsLevel else { return false }
        ()
    default: return false
    }
    
    let lhsInfo = lhs.info
    let rhsInfo = rhs.info
    return lhsInfo.playerName == rhsInfo.playerName && lhsInfo.gameboardID == rhsInfo.gameboardID && lhsInfo.score == rhsInfo.score
}

extension CreateGameRequest: Equatable {}
public func ==(lhs: CreateGameRequest, rhs: CreateGameRequest) -> Bool {
    return lhs.player1Info == rhs.player1Info && lhs.player2Info == rhs.player2Info && lhs.startingLogic == rhs.startingLogic
}

extension CreateGameResponse: Equatable {}
public func ==(lhs: CreateGameResponse, rhs: CreateGameResponse) -> Bool {
    return lhs.player1GameToken == rhs.player1GameToken && lhs.player2GameToken == rhs.player2GameToken
}

extension BatchCreateGameRequest: Equatable {}
public func ==(lhs: BatchCreateGameRequest, rhs: BatchCreateGameRequest) -> Bool {
    guard lhs.requestID == rhs.requestID, lhs.createGameInfos.count == rhs.createGameInfos.count else { return false }
    for keyValuePair in lhs.createGameInfos {
        guard keyValuePair.value == rhs.createGameInfos[keyValuePair.key] else { return false }
    }
    return true
}

extension BatchCreateGameResponse: Equatable {}
public func ==(lhs: BatchCreateGameResponse, rhs: BatchCreateGameResponse) -> Bool {
    guard lhs.requestID == rhs.requestID, lhs.createGameResponses.count == rhs.createGameResponses.count else { return false }
    for keyValuePair in lhs.createGameResponses {
        guard keyValuePair.value == rhs.createGameResponses[keyValuePair.key] else { return false }
    }
    return true
}
