import XCTest
@testable import mancala_cloud_coreTests

XCTMain([
    testCase(mancala_cloud_coreTests.allTests),
])
