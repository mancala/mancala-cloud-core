// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "MancalaCloudCore",
    products: [
        .library(
            name: "MancalaCloudCore",
            targets: ["MancalaCloudCore"])
    ],
    dependencies: [
        .package(url: "git@gitlab.com:mancala/mancala-core.git", from: "1.0.0"),
        .package(url: "git@gitlab.com:mancala/gaming-cloud-core.git", from: "1.1.0"),
    ],
    targets: [
        .target(
            name: "MancalaCloudCore",
            dependencies: ["GamingCloudCore", "MancalaCore"])
    ]
)
